/*
The sketch demonstrates how to do accept a Bluetooth Low Energy 4
Advertisement connection with the RFduino, then send CPU temperature
updates once a second.

It is suppose to be used with the rfduinoTemperature iPhone application.
*/

#include <RFduinoBLE.h>
#include <OBD.h>
#include <Wire.h>
#include <config.h>


COBD obd;

// variables used in packet generation
int ch;
int packet;

int start;

bool connected = false;
// loop interval
int runningInterval = 7200;
int sleepInterval = 3600;
int interval = runningInterval;

// JSON Chars
String startString = "{";
String endString = "}";
String assignString = ":";
String seperatorString = ",";
String quoteString = "'";
//Fields
String speedName = "speed";
String throttelName = "trottel";
String gearName = "gear";
String rpmName = "rpm";
String timeName = "time";
String tempName = "temp";

String startOfString = "##";
String endOfString = "##";



void setup() {
  // this is the data we want to appear in the advertisement
  // (the deviceName length plus the advertisement length must be <= 18 bytes)
  RFduinoBLE.txPowerLevel = +4; 
  RFduinoBLE.advertisementData = "OBD";
  RFduinoBLE.advertisementInterval = 50;
  RFduinoBLE.deviceName = "Ekoio L-PH1";
  // start the BLE stack
  RFduinoBLE.begin();
  Serial.begin(9600);
  if (SERIAL_OUTPUT) {
    Serial.println("setup done");
  
    while (!Serial) {
      ; // wait for serial port to connect. Needed for Leonardo only
    }
  }
}

void loop() {
  // sample once per second
  if (SERIAL_OUTPUT) {
    String start = "start run"; 
    Serial.println(start);
  }
  
  //set imer for sleeping
  //if (!connected) {
  //  RFduino_ULPDelay(sleepInterval);
  //  return;
  //}
  
  int speed = random(0, 180);
  int throttel = 0;
  int gear = random(-1, 6);
  int rpm = random(0, 8000);
  int temp = int(RFduino_temperature(CELSIUS)); 

  String obdData;
  
  obdData += startOfString;
  
  obdData += startString;
  //setSpeadData
  obdData += speedName + assignString + speed + seperatorString;
  // set RpmData
  obdData += rpmName + assignString + rpm + seperatorString;
  // set GearData
  obdData += gearName + assignString + gear + seperatorString;
  // set ThrottleData
  obdData += throttelName + assignString + throttel + seperatorString;
  // timer
  obdData += timeName + assignString + millis() + seperatorString;
  // rfduino temperature
  obdData += tempName + assignString + temp + seperatorString;
  // close JSON 
  obdData += endString;
  
  obdData += endOfString;
  
  //Serial.println(obdData);
  
  int bufferLength = 18;
  
  //put to BLE
  unsigned int len =  obdData.length();
  len++;
  char data[len];
  //char filler[] = " ";
  obdData.toCharArray(data, len);
  //send data via BLE
  Serial.println("Length:");
  Serial.println(len);
  
  for (int i = 0; i < len; i += bufferLength) {
    Serial.println("postion:");
    Serial.println(i);
    char buffer[bufferLength];
    for (int j = 0; j < bufferLength; j++) {
      if (i < len) { 
        buffer[j] = data[ i + j];
        while (! RFduinoBLE.send(buffer, bufferLength))
        ;
      }
    }
    Serial.println("Buffer:");
    Serial.println(buffer);
    
  }
  
  /*for (int j = 0; j < len; j+=bufferLength) {
    //init buffer
    char buffer[bufferLength];
    for (int i = 0; i < bufferLength; i++) {
      if (j+1 >= len) {
        buffer[i] = filler[0];
      } else {
        buffer[i] = data[i+j];
      }
      Serial.println("###################");
       Serial.println(i);
       Serial.println(j);
       Serial.println(data[i+j]);
    }
    Serial.println(buffer);
    RFduinoBLE.send(buffer, bufferLength);
    //while (! RFduinoBLE.send(buffer, 20))
    //;
  }*/
  
  // send is queued (the ble stack delays send to the start of the next tx window)
 
  
  if (SERIAL_OUTPUT) {
    Serial.println(data);
  }
}

void RFduinoBLE_onConnect()
{ 
  packet = 0;
  ch = 'A';
  start = 0;
  if (SERIAL_OUTPUT) {
    Serial.println("connect");
  }
  connected = true;
   RFduino_ULPDelay(runningInterval);
} 

void RFduinoBLE_onDisconnect()
{ 
  if (SERIAL_OUTPUT) {
    Serial.println("disconnect");
  }
  connected = false;
} 

void RFduinoBLE_onReceive(char *data, int len)
{ 
  uint8_t myByte = data[0]; // store first char in array to myByte 
  if (SERIAL_OUTPUT) {  
    Serial.println(myByte); // print myByte via serial 
  }
}
